# README #

This README would normally document whatever steps are necessary to integrate Uninstall Intelligence SDK(FloSDK).

### What is this repository for? ###

Uninstall Intelligence SDK 

Minimum android version required - (minSdkVersion 14)

### How do I get set up? ###

Step 1 - Simply add library to your project 

			/* For gradle user */

			dependencies 
			{
				compile 'io.edgeanalytics:uninstallintelligence:+' // For Latest  
			}
			
			 repositories
			 {
				maven 
				{
					url  "http://dl.bintray.com/edgeanalytics/maven"
				}
   			}

			/* For Maven User */

			<dependency>
			  <groupId>io.edgeanalytics</groupId>
			  <artifactId>uninstallintelligence</artifactId>
			  <version>1.0.10-beta</version>
			  <type>pom</type>
			</dependency>

			/* For Ivy User */

			<dependency org='io.edgeanalytics' name='uninstallintelligence' rev='1.0.10-beta'>
			  <artifact name='uninstallintelligence' ext='pom' ></artifact>
			</dependency>

Step 2 -  Add following code to your Maniefest File

           /* (Mandatory) */
           <uses-permission android:name="android.permission.INTERNET" />

		   
		    /* (Optional) */
			<uses-permission android:name="android.permission.ACCESS_WIFI_STATE" />
            <uses-permission android:name="android.permission.READ_PHONE_STATE" />
		    <uses-permission android:name="android.permission.RECEIVE_BOOT_COMPLETED" />
            
			
			<uses-permission
                     android:name="android.permission.PACKAGE_USAGE_STATS"
                     tools:ignore="ProtectedPermissions" />
	        <uses-permission android:name="android.permission.READ_CALL_LOG" />

		   
		   <service
            android:name=".services.MyDataUsageService"
            android:enabled="true"
            android:exported="true"
            android:stopWithTask="false" />

           <service
            android:name=".services.MyService"
            android:enabled="true"
            android:exported="true"
            android:process=":remote"
            android:stopWithTask="false" />
			
			/* (Optional) */
			
Step 3 -   Download folder add it to your app pacakge folder services.zip 
[Services](https://bitbucket.org/Grku2opia/uninstallsdk/raw/e3e52d6c2132757f8afb289fd9e3c5d50832c52b/services.zip)    

Step 4 -   Initialize SDK in your Application File or Main Activity File



		   //TOKEN_GIVEN_BY_UNINSTALL_SEPARATELY// Given at time of registration
           //APP_SECRET_GIVEN_BY_UNINSTALL_SEPARATELY // Given when add new app to your account
		   FloSDK.initFloSDK(getApplicationContext(),APP_SECRET_GIVEN_BY_UNINSTALL_SEPARATELY,TOKEN_GIVEN_BY_UNINSTALL_SEPARATELY);
		   
		   //Set user personal Information to identify him.
		   
		   FloSDK.setUserEmail(YOUR APPLICATION CONTEXT, USER EMAIL ADDRESS); 
		   FloSDK.setUserMobileNumber(Context context, USER MOBILE NUMBER);
           FloSDK.setUserGender(YOUR APPLICATION CONTEXT, USER GENDER); // FloSDK.GENDER_MALE, FloSDK.GENDER_FEMALE, FloSDK.GENDER_OTHER
		   FloSDK.setUserAge(YOUR APPLICATION CONTEXT, USER AGE); //Age in integer
		   FloSDK.setUserCity(YOUR APPLICATION CONTEXT, USER CITY);
		   
		   
		   //Log your App events using
		   
		   FloSDK.logEvent(YOUR APPLICATION CONTEXT, EVENT)
		   
		   //to create new Event use
		   
		   Event event = new Event();
		   event.setEventName(NAME_OF_EVENT);
		   event.setEventCategory(CATEGORY_OF_EVENT);
		   event.setEventValue(VALUE_OF_EVENT);
		   
		   
		   
Step 5 - Only applicable if you are using proguard

			-keepdirectories com.u2opiamobile.flosdk.models
			-keep class com.u2opiamobile.flosdk.models.** {
  				public protected private *;
			}
			-keep class com.u2opiamobile.flosdk.enums.**{
				public protected private *;
			}
		   
		   




### Who do I talk to? ###

For more info contact us  : support@edgeanalytics.io or admin@edgeanalytics.io